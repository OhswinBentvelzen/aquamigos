﻿package
{
	import flash.display.MovieClip;
	import flash.events.*;
	
	public class Buttons extends MovieClip
	{
		//global button defs
		private var btn_glow:HUDButton_Glow;
		private var btn_press:HUDButton_Press;
		private var menu_glow:HUDMenu_Glow;
		private var menu_press:HUDMenu_Press;
		
		//button defs
		private var btn_food:HUDButton;
		private var food:Food;
		private var foodglow:Food_Glow;
		
		private var btn_profile:HUDButton;
		private var profile:Profile;
		private var profileglow:Profile_Glow;
		
		private var btn_save:HUDButton;
		private var save:Save;
		private var saveglow:Save_Glow;
		
		private var btn_mute:HUDButton;
		private var mute:Mute;
		private var muteglow:Mute_Glow;
		private var rodestreep:Streep;
		
		private var btn_menu:HUDMenu;
		private var menu:HUDTekst;
		private var menuglow:HUDMenu_Glow;

		public function Buttons()
		{
			btn_food = new HUDButton();
			food = new Food();
			foodglow = new Food_Glow();
			
			btn_profile = new HUDButton();
			profile = new Profile();
			profileglow = new Profile_Glow();
			
			btn_save = new HUDButton();
			save = new Save();
			saveglow = new Save_Glow();
			
			btn_mute = new HUDButton();
			mute = new Mute();
			muteglow = new Mute_Glow();
			
			btn_menu = new HUDMenu();
			menu = new HUDTekst();
			menuglow = new HUDMenu_Glow();
			
			defineButton(btn_food, food, foodglow, 100, 200, 157, 157, false, false);
			defineButton(btn_profile, profile, profileglow ,300, 200, 157, 157, false, false);
			defineButton(btn_save, save, saveglow, 200, 300, 157, 157, false, false);
			defineButton(btn_mute, mute, muteglow, 500, 200, 100, 100, true, false);
			defineButton(btn_menu, menu, menuglow ,200, 100, 295, 174, false, true);
			
			addChildAt(btn_food, 0);
			addChildAt(btn_profile, 0);
			addChildAt(btn_save, 0);
			addChildAt(btn_mute, 0);
			addChildAt(btn_menu, 0);
		}
		
		function defineButton(newBtn:MovieClip, tekst:MovieClip, glow:MovieClip, btnx:int, btny:int, btnw:int, btnh:int, isMute:Boolean, isMenu:Boolean)
		{
			newBtn.addEventListener(MouseEvent.MOUSE_OVER, hoover);
			newBtn.addEventListener(MouseEvent.MOUSE_OUT, hoover_out);
			newBtn.addEventListener(Event.ENTER_FRAME, hoover_shade);
			newBtn.addEventListener(MouseEvent.MOUSE_DOWN, pressed);
			newBtn.addEventListener(MouseEvent.MOUSE_UP, released);
			
			newBtn.x = btnx;
			newBtn.y = btny;
			newBtn.width = btnw;
			newBtn.height = btnh;
			
			if(isMute == false)
				newBtn.addChild(tekst);
			else{
				tekst.width = 99;
				tekst.height = 103,5;
				newBtn.addChild(tekst);
			}
			//newBtn.addChild(tekst);
			
			btn_glow = new HUDButton_Glow();
			btn_press = new HUDButton_Press();

			menu_glow = new HUDMenu_Glow();
			menu_press = new HUDMenu_Press();

			var hooving:Boolean;
			
			function hoover()
			{
				hooving = true;
			}
			function hoover_out()
			{
				hooving = false;
			}
			
			function hoover_shade()
			{
				if(hooving == true){
					//newBtn.addChildAt(btn_glow, getChildIndex(newBtn) );
					newBtn.addChild(glow);
					
					if(isMute == false)
						newBtn.addChild(tekst);
					else{
						tekst.width = 99;
						tekst.height = 103,5;
						newBtn.addChild(tekst);
					}
				}
				else
				{
					if(newBtn.contains(btn_glow)){
						newBtn.removeChild(btn_glow);
					}
					if(newBtn.contains(glow)){
						newBtn.removeChild(glow);
					}
					if(newBtn.contains(btn_press)){
						newBtn.removeChild(btn_press);
					}
					if(newBtn.contains(menu_press)){
						newBtn.removeChild(menu_press);
					}
					
				}
			}
			var muteOn:Boolean;
			rodestreep = new Streep();
			
			function pressed()
			{
				if(isMenu == false)
					newBtn.addChild(btn_press);
				else
					newBtn.addChild(menu_press);
				
			}
			function released()
			{
				if(newBtn.contains(btn_press))
					newBtn.removeChild(btn_press);
					
				if(isMute == true)
				{
					if(muteOn == true){
						if(newBtn.contains(rodestreep))
							newBtn.removeChild(rodestreep);
						muteOn = false;
					}
					else{
						muteOn = true;
						//newBtn.addChildAt(rodestreep, getChildIndex(newBtn)+2);
						newBtn.addChild(rodestreep);
					}
				}
				if(isMenu == true)
				{
					if (newBtn.contains(menu_press))
							newBtn.removeChild(menu_press);
				}
				
			}
		}
	}
}