﻿package
{
	import flash.display.MovieClip;
	import flash.events.*;
	
	public class MainMenu extends MovieClip
	{
		private var button1:Button1;
		private var button2:Button2;
		private var button3:Button3;
		private var button1_glow:Button1Glow;
		private var button2_glow:Button2Glow;
		private var button3_glow:Button3Glow;
		
		public function MainMenu()
		{
			button1 = new Button1;
			button2 = new Button2;
			button3 = new Button3;
			
			button1_glow = new Button1Glow;
			button2_glow = new Button2Glow;
			button3_glow = new Button3Glow;
			
			btn(button1, 400, 300, button1_glow);
			btn(button2, 380, 620, button2_glow);
			btn(button3, 720, 450, button3_glow);
			
			addChild(button1);
			addChild(button2);
			addChild(button3);
		}
		
		function btn(newButton:MovieClip, btnX:int, btnY:int, newGlow:MovieClip)
		{
			newButton.addEventListener(Event.ENTER_FRAME, bubbles);
			newButton.addEventListener(MouseEvent.MOUSE_OVER, bubbleIn);
			newButton.addEventListener(MouseEvent.MOUSE_OUT, bubbleOut);
			newButton.x = btnX;
			newButton.y = btnY;
			
			//stage.addChild(newButton);
			
			var playing:Boolean;
			
			var blub1:BubbleMove = new BubbleMove();
			var blub2:BubbleMove3 = new BubbleMove3();
			var blub3:BubbleMove = new BubbleMove();
			var blub4:BubbleMove2 = new BubbleMove2();
			var blub5:BubbleMove4 = new BubbleMove4();
			//var glow1:MovieClip = new newGlow();
			
			
			blub1.x = -100;
			blub1.y = -50;
			blub1.play();
			
			blub2.x = -5;
			blub2.y = 35;
			blub2.play();
			//newButton.addChild(blub2);
			
			blub3.x = 57;
			blub3.y = 88;
			blub3.play();
			newButton.addChild(blub3);
			
			blub4
			blub4.play();
			//newButton.addChild(blub4);
			
			blub5.play();
			blub5.x = -180;
			
			function bubbleIn(e:MouseEvent):void 
			{
				playing = true;
			}
			
			function bubbleOut(e:MouseEvent):void 
			{
				playing = false;
			}
			
			function bubbles(e:Event):void
			{
				if(playing == true)
				{		
					newButton.addChild(blub1);
					newButton.addChild(blub2);
					newButton.addChild(blub3);
					newButton.addChild(blub4);
					newButton.addChild(blub5);
					newButton.addChild(newGlow);
				}
				if(playing == false)
				{
					if(newButton.contains(blub1)){
						newButton.removeChild(blub1);}
					if(newButton.contains(blub2)){
						newButton.removeChild(blub2);}
					if(newButton.contains(blub3)){
						newButton.removeChild(blub3);}
					if(newButton.contains(blub4)){
						newButton.removeChild(blub4);}
					if(newButton.contains(blub5)){
						newButton.removeChild(blub5);}
					if(newButton.contains(newGlow)){
						newButton.removeChild(newGlow);}
				}
			}
		}
	}
}